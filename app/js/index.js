document.addEventListener('DOMContentLoaded', function () {
    // Keys
    var KEY_UP = 38;
    var KEY_DOWN = 40;
    var KEY_ENTER = 13;

    var autoComplete = new AutoComplete();
    var prevValue = '';

    /**
     * Select an answer option by clicking on an item
     */
    document.addEventListener('click', function(e) {
        if (hasClass(e.target, autoComplete.CLASS_ITEM)) {
            autoComplete.selectOption();
        }
    }, true);

    /**
     * Keyboard input
     */
    autoComplete.$input.addEventListener('keyup', function (e) {
        // Using 'keyup' instead of 'change' because IE9 does not fire 'change' event on remove symbols
        if (prevValue !== this.value) {
            prevValue = this.value;
            removeClass(this.parentElement, autoComplete.CLASS_ERROR);
            autoComplete.clearSuggestList();

            if (autoComplete.isFound) {
                autoComplete.isFound = false;
                return;
            }

            return autoComplete.search(this.value);
        }

        /**
         * Select option by using keys 'up' and 'down' and 'Enter'
         * @type {*|Event | undefined}
         */
        e = e || window.event;
        switch (e.keyCode) {
            case KEY_UP:
                autoComplete.prevOption();
                break;

            case KEY_DOWN:
                autoComplete.nextOption();
                break;

            case KEY_ENTER:
                autoComplete.selectOption();
                break;
        }
    }, true);

    /**/
    autoComplete.$input.addEventListener('focus', function () {
        this.select();
        removeClass(this.parentElement, autoComplete.CLASS_ERROR);
        addClass(this.parentElement, autoComplete.CLASS_IN_FOCUS);

        // for previous values
        if (this.value.length) {
            return autoComplete.search(this.value);
        }

        autoComplete.clearSuggestList();
        autoComplete.reset();
    }, false);

    /**/
    autoComplete.$input.addEventListener('blur', function () {
        // Timer user on purpose
        // That's bad idea but I didn't find another solution to catch click on selected option
        setTimeout(handleBlur, 250);
    }, false);

    /**/
    function handleBlur() {
        var $parent = autoComplete.$input.parentElement;
        removeClass($parent, autoComplete.CLASS_IN_FOCUS);

        var $active = autoComplete.getActiveOption();
        var $options = autoComplete.$list.getElementsByClassName(autoComplete.CLASS_ITEM);
        autoComplete.reset();
        // Select a option if there is only one found
        if ($active.length && ($options.length === 1)) {
            autoComplete.selectOption();
            return;
        }

        if (false === autoComplete.isFound) {
            addClass($parent, autoComplete.CLASS_ERROR);
        }
    }

    /**/
    autoComplete.$list.addEventListener('mouseover', function (e) {
        var hoverItem = e.target;
        if (hasClass(hoverItem, autoComplete.CLASS_ACTIVE_ITEM)) {
            return;
        }

        var activeItem = hoverItem.parentElement.querySelector('.' + autoComplete.CLASS_ACTIVE_ITEM);
        removeClass(activeItem, autoComplete.CLASS_ACTIVE_ITEM);
        addClass(hoverItem, autoComplete.CLASS_ACTIVE_ITEM);
    }, true);
}, false);
