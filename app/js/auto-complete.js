/**
 * @constructor
 */
var AutoComplete = function () {
    // State
    this.isReady = false;
    this.isFound = false;

    // Constants
    this.NUMBER_OF_RESULTS = 10;
    this.JSON_URL = 'cities.json';

    this.CLASS_LIST = 'suggest-list--visible';
    this.CLASS_ITEM = 'suggest-list__item';
    this.CLASS_HINT = 'suggest-list__hint';
    this.CLASS_ACTIVE_ITEM = 'suggest-list__item--active';
    this.CLASS_LOADING = 'autocomplete--loading';
    this.CLASS_IN_FOCUS = 'autocomplete--in-focus';
    this.CLASS_ERROR = 'autocomplete--with-error';
    this.CLASS_NOT_FOUND = 'autocomplete--not-found';

    // Elements
    this.$self = document.getElementById('custom-autocomplete');
    this.$input = this.$self.querySelector('.js-input');
    this.$window = this.$self.querySelector('.js-window');
    this.$list = this.$self.querySelector('.js-list');

    // Data
    this.cities = [];
    this.timeout = null;

    var self = this;

    function handleResponse(response) {
        self.cities = JSON.parse(response);
        self.isReady = true;
    }

    loadJSON(this.JSON_URL, handleResponse);
};

/**
 * @param {string} str
 * @returns {array}
 */
AutoComplete.prototype.search = function (str) {
    var self = this;
    var re = new RegExp('^' + str, 'i');
    self.reset();
    self.clearSuggestList();
    addClass(self.$window, self.CLASS_LOADING);

    // Primitive throttling
    clearTimeout(self.timeout);
    self.timeout = setTimeout(function () {
        removeClass(self.$window, self.CLASS_LOADING);
        var arr = [];
        if (self.isReady && str.length) {
            arr = self.cities.filter(findByIdOrCity);
        }

        self.showResult(arr);
    }, 1000);

    /**
     * @param item
     * @returns {boolean}
     */
    function findByIdOrCity(item) {
        return (item.hasOwnProperty('City') && re.test(item.City))
            || (item.hasOwnProperty('Id') && re.test(item.Id));
    }

};

/**
 * @param arr
 */
AutoComplete.prototype.showResult = function (arr) {
    var self = this;
    var len = arr.length;
    if (len === 0) {
        return self.showNotFound();
    }

    addClass(self.$list, self.CLASS_LIST);
    /**
     * Render found options
     * Shown just #NUMBER_OF_RESULTS# options if there are too many options found
     */
    arr.slice(0, self.NUMBER_OF_RESULTS)
        .forEach(function (el, index) {
            var item = document.createElement('li');
            var className = self.CLASS_ITEM;
            if (0 === index) {
                className += ' ' + self.CLASS_ACTIVE_ITEM;
            }

            item.setAttribute('class', className);
            item.textContent = el.City;
            self.$list.appendChild(item);
        });

    /**
     * A hint about the number of results found
     */
    if (arr.length > self.NUMBER_OF_RESULTS) {
        var hint = document.createElement('li');
        hint.setAttribute('class', self.CLASS_HINT);
        hint.textContent = 'Показано ' + self.NUMBER_OF_RESULTS + ' из ' + len + ' найденных инспекций. ';
        hint.textContent += 'Уточните запрос, чтобы увидеть остальные';
        self.$list.appendChild(hint);
    }
};

/**/
AutoComplete.prototype.clearSuggestList = function () {
    this.$list.innerHTML = '';
};

/**
 * @returns {NodeListOf<Element>}
 */
AutoComplete.prototype.getActiveOption = function () {
    return this.$list.getElementsByClassName(this.CLASS_ACTIVE_ITEM);
};

/**/
AutoComplete.prototype.nextOption = function () {
    var $active = this.getActiveOption()[0];
    var $next = $active.nextSibling;
    if ((null != $next) && hasClass($next, this.CLASS_ITEM)) {
        addClass($next, this.CLASS_ACTIVE_ITEM);
        removeClass($active, this.CLASS_ACTIVE_ITEM);
    }
};

/**/
AutoComplete.prototype.prevOption = function () {
    var $active = this.getActiveOption()[0];
    var $prev = $active.previousElementSibling;
    if (null != $prev) {
        addClass($prev, this.CLASS_ACTIVE_ITEM);
        removeClass($active, this.CLASS_ACTIVE_ITEM);
    }
};

/**/
AutoComplete.prototype.selectOption = function () {
    var $active = this.getActiveOption()[0];
    if ((null != $active) && hasClass($active, this.CLASS_ACTIVE_ITEM)) {
        this.$input.value = $active.textContent;
        this.isFound = true;
        this.clearSuggestList();
        this.reset();
    }
};

/**/
AutoComplete.prototype.getActiveOption = function () {
    return this.$list.getElementsByClassName(this.CLASS_ACTIVE_ITEM);
};

/**/
AutoComplete.prototype.showNotFound = function () {
    addClass(this.$window, this.CLASS_NOT_FOUND);
};

/**
 * Reset suggest window to initial state
 */
AutoComplete.prototype.reset = function () {
    removeClass(this.$list, this.CLASS_LIST);
    removeClass(this.$window, this.CLASS_LOADING);
    removeClass(this.$window, this.CLASS_ERROR);
    removeClass(this.$window, this.CLASS_NOT_FOUND);
};
